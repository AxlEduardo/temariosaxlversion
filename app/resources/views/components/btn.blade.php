<button class="
    p-1.5 bg-teal-500 rounded-lg shadow-lg 
    text-white hover:text-teal-500 hover:bg-gray-100
    hover:scale-105 ring-2 ring-green-300
">
    {{ $slot }}
</button>