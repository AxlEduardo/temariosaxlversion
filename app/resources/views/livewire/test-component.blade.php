<div>
    Soy un componente
    <br />
    <input type="text" wire:model.live='nombre' />
    <br />{{ $nombre }}
    <br>
    <button>Boton normal</button>
    <br>
    <x-button>X-Boton</x-button>
    <br>
    <x-btn>Mi X-Button</x-btn>
    <br>
    <x-dummy-component>Soy un X-Component con View</x-dummy-component>
</div>
