<div class="p-1.5">
    <div class="p-3">
        @livewire('asignaturas.guardar-asignatura-component')
    <button type="button" wire:click='openModal()' class="p-3 bg-white border-b-4 border-blue-700 text-blue-700 rounded-md shadow-md hover:bg-blue-700 hover:text-white hover:border-blue-800">
        Nueva asignatura
    </button>

    </div>
    <x-action-message on='success' class="w-full p-3 bg-teal-300 text-center text-teal-600 text-lg">
        Se guardó la materia
    </x-action-message>
    <x-action-message on='materiaEliminada' class="w-full p-3 bg-red-300 text-center text-white text-lg">
        Materia eliminada correctamente
    </x-action-message>
    <x-action-message on='errorDeleting' class="w-full p-3 bg-red-500 text-center text-white text-lg">
    Error al eliminar la materia.
</x-action-message>

    <div class="p-1 mt-3 grid grid-cols-2">
        <div class="col-span-1">
            Asignaturas:
            <input type="search" class="p-1 ml-1.5 border-2 border-blue-100 rounded-md" wire:model.live='search' placeholder="Buscar..." />
        </div>
        <div class="col-span-1 flex justify-end">
            <label for="paginacion">Registros por página:
                <select id="paginacion" wire:model.live='rows'>
                    <option value="2">2</option>
                    <option value="5">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
            </label>
        </div>
    </div>
    <br>
    <table class="p-1 w-full">
    <thead class="bg-gray-100">
        <tr>
            <th class="p-3">#</th>
            <th class="p-3">Materia</th>
            <th class="p-3">Editar</th>
            <th class="p-3">Eliminar</th>
        </tr>
    </thead>
    <tbody class="text-center">
        @foreach ($materias as $m)
            <tr class="border-b-2 border-gray-300">
                <td class="p-3">{{ $m->id_materia }}</td>
                <td class="p-3">{{ $m->nombre_materia }}</td>
                <td class="p-3">
                    <button wire:click='openModal({{ $m->id_materia }})' class="py-1.5 px-1 bg-gray-300 text-xs rounded-md">Editar</button>
                </td>
                <td class="p-3">
                    <button wire:click='deleteMateria({{ $m->id_materia }})' class="py-1.5 px-1 bg-red-300 text-xs rounded-md">Eliminar</button>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
    <div class="p-3 m-3">
        {{ $materias->links() }}
    </div>
</div>

@push('scripts')
<script src="{{ asset('js/livewire-events.js') }}"></script>
@endpush

