<?php

use App\Http\Controllers\Asignaturas\AsignaturasController;
use App\Livewire\Asignaturas\ListAsignaturasComponent;
use Illuminate\Support\Facades\Route;
use Livewire\Livewire;

Livewire::setUpdateRoute(function ($handle) {
    return Route::post('/temarios-live/livewire/update', $handle);
});
Livewire::setScriptRoute(function ($handle) {
    return Route::get('/temarios-live/livewire/livewire.js', $handle);
});


Route::get('/', function () {
    return view('welcome');
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');

    Route::any('asignaturas', [AsignaturasController::class, 'index'])
        ->name('asignaturas');

});
