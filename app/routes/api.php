<?php

use App\Http\Controllers\API\AsignaturasAPIController;
use App\Http\Controllers\API\MiAPIController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');

Route::apiResource('asignaturas', AsignaturasAPIController::class);
Route::post('edit-asignatura', [AsignaturasAPIController::class, 'edit'])->name('asignatura.edit');
Route::get('materias', [MiAPIController::class, 'index'])->name('materias');
Route::post('login', [MiAPIController::class, 'login'])->name('login');
