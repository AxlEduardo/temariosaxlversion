<?php

namespace App\Models\Asignaturas;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Materia extends Model
{
    use HasFactory;

    protected $table = 'materias';
    protected $primaryKey = 'id_materia';

    protected $fillable = [
        'nombre_materia',
        'clave_materia',
        'horas_teoricas',
        'horas_practicas',
        'creditos',
        'carrera',
    ];
}
