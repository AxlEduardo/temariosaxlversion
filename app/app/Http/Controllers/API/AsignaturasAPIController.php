<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Asignaturas\Materia;
use Illuminate\Http\Request;

class AsignaturasAPIController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $asignaturas = Materia::orderBy('id_materia')->get();

        return response($asignaturas, 200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'nombre_materia' => 'required|min:3|max:30',
            'clave_materia' => 'required|min:3|max:9',
            'horas_teoricas' => 'required|integer',
            'horas_practicas' => 'required|integer',
            'creditos' => 'required|integer',
            'carrera' => 'nullable|min:3|max:30'
        ]);

        $materia = new Materia();
        $materia->fill($validated);
        $materia->save();

        return response($materia, 200);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $asignatura = Materia::find($id);

        return response($asignatura, 200);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $validated = $request->validate([
            'nombre_materia' => 'required|min:3|max:30',
            'clave_materia' => 'required|min:3|max:9',
            'horas_teoricas' => 'required|integer',
            'horas_practicas' => 'required|integer',
            'creditos' => 'required|integer',
            'carrera' => 'nullable|min:3|max:30'
        ]);

        try {
            $materia = Materia::find($id);

            $materia->fill($validated);
            $materia->save();

            return response($materia, 200);
        } catch (\Exception $ex) {
            info($ex->getMessage());
            return response('Error al eliminar', 406);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $asignatura = Materia::find($id);

            $asignatura->delete();

            return response('Elimninado correctamente', 200);
        } catch (\Exception $ex) {
            info($ex->getMessage());
            return response('Error al eliminar', 406);
        }

    }

    public function edit(Request $request) {
        $validated = $request->validate([
            'id' => 'required|integer',
            'nombre_materia' => 'required|min:3|max:30',
            'clave_materia' => 'required|min:3|max:9',
            'horas_teoricas' => 'required|integer',
            'horas_practicas' => 'required|integer',
            'creditos' => 'required|integer',
            'carrera' => 'nullable|min:3|max:30'
        ]);

        try {
            $materia = Materia::find($request->id);

            $materia->fill($validated);
            $materia->save();

            return response($materia, 200);
        } catch (\Exception $ex) {
            info($ex->getMessage());
            return response('Error al eliminar', 406);
        }
    }

}
