<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Asignaturas\Materia;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class MiAPIController extends Controller
{
    public function index() {
        $asignaturas = Materia::orderBy('id_materia')->paginate(3);

        return response($asignaturas, 200);
    }

    public function login(Request $request) {
        $validated = $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if (Auth::attempt($validated)) {
            return response()->json([
                'token' => $request->user()->createToken('MiToken')->plainTextToken
            ]);
        } else {
            return response('', 401);
        }
    }

}
