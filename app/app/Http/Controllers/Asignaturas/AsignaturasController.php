<?php

namespace App\Http\Controllers\Asignaturas;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AsignaturasController extends Controller
{
    function index() {
        return view('asignaturas.asignaturas');
    }
}
