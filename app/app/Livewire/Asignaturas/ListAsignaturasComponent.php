<?php

namespace App\Livewire\Asignaturas;

use Livewire\Component;

use App\Models\Asignaturas\Materia;
use Livewire\Attributes\On;
use Livewire\WithPagination;

class ListAsignaturasComponent extends Component
{
    use WithPagination;

    public $search;
    public $rows = 10;

    public function render()
    {
        $materias = Materia::orderBy('id_materia');

        if($this->search) {
            $materias = $materias->where('nombre_materia', 'ILIKE', '%'.$this->search.'%');
        }
        $materias = $materias->paginate($this->rows);

        return view(
            'livewire.asignaturas.list-asignaturas-component',
            compact('materias')
        );
    }

    public function updatingSearch() {
        $this->resetPage();
    }

    public function openModal($id = null) {
        if($id) {
            $this->dispatch('open-modal', ['id' => $id]);
        } else {
            $this->dispatch('open-modal');
        }
    }

    #[On('guardado')]
    public function refresh() {
        $this->resetPage();
        $this->dispatch('success');
    }

   public function deleteMateria($id)
{
    try {
        $materia = Materia::findOrFail($id);
        $materia->delete();

        // Emitir un evento a Livewire para notificar que la materia fue eliminada correctamente
        $this->dispatch('materiaEliminada');
    } catch (\Throwable $e) {
        // Emitir un evento de error
        $this->emit('errorDeleting', 'Error al eliminar la materia: ' . $e->getMessage());
    }
}



}
