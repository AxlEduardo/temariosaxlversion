<?php

namespace App\Livewire\Asignaturas;

use App\Livewire\Forms\Asignaturas\AsignaturaForm;
use App\Models\Asignaturas\Materia;

use Livewire\Component;
use Livewire\Attributes\On; 

class GuardarAsignaturaComponent extends Component
{
    public $modalGuardar = false;
    public $title = 'Nueva materia';

    public AsignaturaForm $form;

    public function render()
    {
        return view('livewire.asignaturas.guardar-asignatura-component');
    }

    #[On('open-modal')] 
    public function abrirModal($id = null) {
        $materia = new Materia();
        if($id) {
            $materia = Materia::where('id_materia', $id)->first();
            $this->form->set($materia);
        } else {
            $this->form->clear();
        }

        $this->modalGuardar = true;
    }

    public function save() {
        if($this->form->save()) {
            $this->dispatch('guardado');
        }
        $this->modalGuardar = false;
    }

}
