<?php

namespace App\Livewire\Forms\Asignaturas;

use Livewire\Attributes\Validate;
use Livewire\Form;

use App\Models\Asignaturas\Materia;

class AsignaturaForm extends Form
{
    public $id_materia;

    #[Validate('required|min:3|max:90')]
    public $nombre_materia;

    #[Validate('required|min:3|max:9')]
    public $clave_materia;

    #[Validate('required|integer|digits:1')]
    public $horas_teoricas;

    #[Validate('required|integer|digits:1')]
    public $horas_practicas;

    #[Validate('required|integer|digits:1')]
    public $creditos;

    #[Validate('nullable|min:3|max:120')]
    public $carrera;

    function set(Materia $materia) 
    {
        $this->id_materia = $materia->id_materia;
        $this->nombre_materia = $materia->nombre_materia;
        $this->clave_materia = $materia->clave_materia;
        $this->horas_teoricas = $materia->horas_teoricas;
        $this->horas_practicas = $materia->horas_practicas;
        $this->creditos = $materia->creditos;
        $this->carrera = $materia->carrera;
    }

    function clear() {
        $this->reset();
    }

    function save($id = null)
    {
        $validated = $this->validate();

        try {
            $materia = Materia::findOrNew($this->id_materia);
            $materia->fill($validated);
            $materia->save();

            $this->reset();

            return $materia;
        } catch(\Exception $ex) {
            info($ex->getMessage());
            $this->addError('Materia', 'Error al guardar la materia');
            return null;
        }
    }
}
