<?php

namespace Database\Seeders;

use App\Models\Asignaturas\Materia;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MateriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Materia::create([
            'nombre_materia' => 'POO',
            'clave_materia' => 'ISC-007',
            'horas_teoricas' => 2,
            'horas_practicas' => 3,
            'creditos' => 5,
            'carrera' => 'Sistemas',
        ]);

        $materia = new Materia();
        $materia->nombre_materia = 'TAP';
        $materia->clave_materia = 'ISC-012';
        $materia->horas_teoricas = 2;
        $materia->horas_practicas = 3;
        $materia->creditos = 5;
        $materia->carrera = 'Sistemas';
        $materia->save();

        $materia = new Materia();
        $materia->nombre_materia = 'ICPW';
        $materia->clave_materia = 'ISC-015';
        $materia->horas_teoricas = 2;
        $materia->horas_practicas = 3;
        $materia->creditos = 5;
        $materia->carrera = 'Sistemas';
        $materia->save();
    }
}
