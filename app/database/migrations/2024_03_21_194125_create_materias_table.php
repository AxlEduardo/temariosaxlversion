<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('materias', function (Blueprint $table) {
            $table->id('id_materia');
            $table->string('nombre_materia', 120)->comment('Es el nombre de la materia');
            $table->string('clave_materia', 18)->comment('Clave de la materia');
            $table->integer('horas_teoricas')->default(0)->comment('Horas teoricas de la materia');
            $table->integer('horas_practicas')->default(0)->comment('Horas practicas de la materia');
            $table->integer('creditos')->default(0)->comment('Créditos de la materia');
            $table->string('carrera')->nullable()->comment('Carrera a la que aplica la materia');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('materias');
    }
};
