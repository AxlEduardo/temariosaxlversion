<?php

namespace Database\Factories\Asignaturas;

use App\Models\Asignaturas\Materia;
use Illuminate\Database\Eloquent\Factories\Factory;

class MateriaFactory extends Factory
{
    protected $model = Materia::class;

    public function definition()
    {
        return [
            'nombre_materia' => $this->faker->word,
            'clave_materia' => strtoupper($this->faker->bothify('???###')),
            'horas_teoricas' => $this->faker->numberBetween(1, 5),
            'horas_practicas' => $this->faker->numberBetween(1, 5),
            'creditos' => $this->faker->numberBetween(1, 10),
            'carrera' => $this->faker->word,
        ];
    }
}
