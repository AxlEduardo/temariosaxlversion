<?php

// tests/Unit/ExampleTest.php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Livewire\Livewire;
use Tests\TestCase;
use App\Livewire\Asignaturas\ListAsignaturasComponent;
use App\Livewire\Asignaturas\GuardarAsignaturaComponent;
use App\Models\User;
use App\Models\Asignaturas\Materia;

class ExampleTest extends TestCase
{
    use RefreshDatabase;

    public function test_that_true_is_true(): void
    {
        $this->assertTrue(true);
    }

    public function test_authenticated_user_can_access_dashboard()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->get('/dashboard');

        $response->assertStatus(200);
    }

    public function test_authenticated_user_can_access_asignaturas()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->get('/asignaturas');

        $response->assertStatus(200);
    }

    public function test_component_can_render()
    {
        Livewire::test(ListAsignaturasComponent::class)
            ->assertStatus(200);
    }

    public function test_search_functionality()
    {
        Materia::factory()->create(['nombre_materia' => 'Matemáticas']);
        Materia::factory()->create(['nombre_materia' => 'Física']);

        Livewire::test(ListAsignaturasComponent::class)
            ->set('search', 'Matemáticas')
            ->assertSee('Matemáticas')
            ->assertDontSee('Física');
    }

    public function test_guardar_component_can_render()
    {
        Livewire::test(GuardarAsignaturaComponent::class)
            ->assertStatus(200);
    }

    public function test_can_open_modal_to_create_new_materia()
    {
        Livewire::test(GuardarAsignaturaComponent::class)
            ->call('abrirModal')
            ->assertSet('modalGuardar', true)
            ->assertSet('title', 'Nueva materia');
    }

    public function test_can_save_materia()
    {
        Livewire::test(GuardarAsignaturaComponent::class)
            ->call('abrirModal')
            ->set('form.nombre_materia', 'Química')
            ->set('form.clave_materia', 'QUI123')
            ->set('form.horas_teoricas', 3)
            ->set('form.horas_practicas', 2)
            ->set('form.creditos', 5)
            ->set('form.carrera', 'Ingeniería Química')
            ->call('save');

        $this->assertDatabaseHas('materias', [
            'nombre_materia' => 'Química',
            'clave_materia' => 'QUI123',
            'horas_teoricas' => 3,
            'horas_practicas' => 2,
            'creditos' => 5,
            'carrera' => 'Ingeniería Química',
        ]);
    }
}

